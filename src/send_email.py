import os
import re
from playwright.sync_api import expect

from dotenv import load_dotenv
load_dotenv()
proton_login_page = os.getenv('PROTON_LOGIN_PAGE')
proton_username = os.getenv('PROTON_USERNAME')
proton_password = os.getenv('PROTON_PASSWORD')
send_email_message_to = os.getenv('SEND_EMAIL_MESSAGE_TO')
send_email_message_subject = os.getenv('SEND_EMAIL_MESSAGE_SUBJECT')
send_email_message_body = os.getenv('SEND_EMAIL_MESSAGE_BODY')


class SendEmail:
    def __init__(self, page):
        self.page = page

    def login(self):
        page = self.page
        page.goto(proton_login_page)

        username_field = page.wait_for_selector('#username')
        password_field = page.wait_for_selector('#password')
        username_field.fill(proton_username)
        password_field.fill(proton_password)
        page.get_by_role("button", name='Sign in').click()

        page.wait_for_selector('//div[contains(@class, "items-column-list-inner")]')

    def send_email(self):
        page = self.page
        page.get_by_test_id('sidebar:compose').click()
        page.get_by_test_id('composer:to').fill(send_email_message_to)
        page.get_by_test_id('composer:subject').fill(send_email_message_subject)
        page.frame_locator('[data-testid=rooster-iframe]').locator('#rooster-editor').fill(send_email_message_subject)
        page.get_by_test_id('composer:send-button').click()

    def email_sent_successfully(self):
        page = self.page
        sent = page.locator('//span[contains(@class, "notification__content")]')
        expect(sent).to_have_text(re.compile(r'Message sent.*'))
