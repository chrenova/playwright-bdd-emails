### Programmatically send emails from the protonmail account
Functionality:
- send email from an existing protonmail account to a given recipient
- bdd test

#### Preconditions
- linux box with python3 installed
- user has an existing email account at protonmail (you can create one at https://proton.me/mail)

#### 1. Checkout project  
```
$ git clone https://gitlab.com/chrenova/playwright-bdd-emails.git
$ cd playwright-bdd-emails
```

#### 2. Create a virtual environment
```
$ python3 -m venv .venv
$ source .venv/bin/activate
```

#### 3. Install dependencies
```
$ pip install -r requirements.txt
```

#### 3.1 Install playwright browsers
- refer to the docs (https://playwright.dev/python/docs/intro)
```
$ playwright install
```

#### 4. Create a '.env' file from template
```
$ cp .env.template .env
```

#### 5. Set up environment variables by editing the new .env file
at least PROTON_USERNAME, PROTON_PASSWORD and SEND_EMAIL_MESSAGE_TO need to be set up properly  
'.env' should look like the following:
```
$ cat .env
PROTON_USERNAME=username
PROTON_PASSWORD=password
PROTON_LOGIN_PAGE=https://account.proton.me/login

SEND_EMAIL_MESSAGE_TO=recipient@domain.com
SEND_EMAIL_MESSAGE_SUBJECT=test
SEND_EMAIL_MESSAGE_BODY=test
```

#### 6. Run the test
```
$ pytest -s
```

#### 7. Expectation
test should run successfully, the given recipient should receive an email
