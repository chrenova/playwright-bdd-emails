import os
import re
import pytest
from pytest_bdd import scenario, given, when, then
from src.send_email import SendEmail
from src.utils import init_playwright

@pytest.fixture
def context():
    with init_playwright() as page:
        context = dict()
        context['page'] = SendEmail(page)
        yield context

@scenario('features/email.feature', 'Send email')
def test_send_email(context):
    pass

@given('I am logged in')
def login(context):
    page = context['page']
    page.login()

@when('I try to send email')
def send(context):
    page = context['page']
    page.send_email()

@then('Email is sent successfully')
def check(context):
    page = context['page']
    page.email_sent_successfully()

